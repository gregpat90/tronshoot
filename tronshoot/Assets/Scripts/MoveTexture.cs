using UnityEngine;
using System.Collections;

public class MoveTexture : MonoBehaviour {

    public float XAnimationSpeed = 0.01f;
    public float YAnimationSpeed = 0.01f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //
        //animate the texture so that the image moves along the mesh
        //
        var meshRenderer = gameObject.GetComponent<MeshRenderer>();
        Vector2 offset = meshRenderer.material.mainTextureOffset;

        offset.x += XAnimationSpeed;
        offset.y += YAnimationSpeed;

        meshRenderer.material.SetTextureOffset("_MainTex", offset);
	}
}
