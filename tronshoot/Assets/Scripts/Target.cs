using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {
    public float RayDistance = 100.0f;

    // Use this for initialization
    void Start () {
    
    }
    
    // Update is called once per frame
    void Update () {
        RaycastHit hitInfo;
        var mousePos = new Vector3
        {
            x = Input.mousePosition.x,
            y = Input.mousePosition.y,
            z = 1.0f
        };
		
		
		
        var ray = Camera.main.ScreenPointToRay(mousePos);
        int layerMask = 1 << GlobalProperties.LayerTargetRayCatcher;

        if (Physics.Raycast(ray, out hitInfo, RayDistance, layerMask))
        {
            transform.position = hitInfo.point;
        }
        else
        {
			Debug.Log("in Target");
            transform.position = Vector3.zero;
        }
    }
}
