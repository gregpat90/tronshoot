using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

public class Loot : MonoBehaviour {
    public enum LootType
    {
        Weapon = 0,
        Health = 1,
		Secret = 2
    };

    public LootType lootType;
	public bool ShowGui = false;
	public static float forcePower = 0.5f;
	public WeaponInfo weaponInfo = new WeaponInfo();
    public int HealthRegain = 50;
    public int EnergySalvage = 10;
	public float SpeedBoost = 1.5f;
    public Texture WeaponTexture;
    public Texture HealthTexture;
	public Texture RandomTexture;

	
	public static void SpawnRandom(Vector3 position){

		Random.seed = Time.frameCount;	
		int count = Random.Range(0,4);
		
		for(int i = 0; i <= count; i++){
			
			GameObject go = Instantiate(Resources.Load("Loot")) as GameObject;
			go.transform.position = position;
			
			Vector2 randomDirection = Random.insideUnitCircle;
			Vector3 direction = new Vector3(randomDirection.x, 1.0f, randomDirection.y);
				
			go.rigidbody.AddForce(direction * forcePower, ForceMode.Impulse);
			
			Loot loot = go.GetComponent<Loot>();
            loot.lootType = (LootType)Random.Range(0, 3);
			loot.weaponInfo.FireRate = Random.Range(0.1f, 0.6f);
			loot.weaponInfo.Damage = Random.Range(5, 50);

            Debug.Log((int)loot.lootType);

            if (loot.lootType == LootType.Weapon)
            {
                loot.renderer.material.SetTexture("_MainTex", loot.WeaponTexture);
            }
            else if (loot.lootType == LootType.Health)
            {
                loot.renderer.material.SetTexture("_MainTex", loot.HealthTexture);
            }
            else if (loot.lootType == LootType.Secret)
            {
                loot.renderer.material.SetTexture("_MainTex", loot.RandomTexture);
            }
		}
	}
	
	 void OnGUI () {
		if(ShowGui){
						
			Vector3 guiCenter = Camera.mainCamera.WorldToScreenPoint(this.transform.position);
			
			Rect windowRect = new Rect( guiCenter.x - 50, Camera.mainCamera.pixelHeight - guiCenter.y - 50, 100, 100);
			
			if(!windowRect.Contains(Input.mousePosition)){
				PlayMakerFSM fsm = this.GetComponent<PlayMakerFSM>();
				if(fsm != null){
					fsm.Fsm.Event("MouseOut"); 
				}
				return;
			}
			
			GUI.BeginGroup (windowRect);
			// All rectangles are now adjusted to the group. (0,0) is the topleft corner of the group.

			// We'll make a box so you can see where the group is on-screen.
			GUI.Box (new Rect (0,0,100,100), "Loot");

			GUI.Label(new Rect(10,40,100,20), string.Format("Fire Rate: {0:f}", weaponInfo.FireRate));
			GUI.Label(new Rect (10,60,100,20), string.Format("Damage: {0:d}", weaponInfo.Damage));

			// End the group we started above. This is very important to remember!
			GUI.EndGroup ();
		}
	}
	
}
