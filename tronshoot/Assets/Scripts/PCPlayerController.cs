using UnityEngine;
using System.Collections;

public class PCPlayerController : MonoBehaviour {

    public float MovementSpeed = 1.0f;
    public float LootRadius = 15.0f;
    //public float ActionTimeDelay = 
    
    public Loot ClosestLoot;
    public bool AutoPickUpHealth = true;
    public Transform LootIn;
    public Transform LootOut;
    public Transform HighlightCube;

    //private float TimeSinceLastAction;

    public enum LootAction
    {
        None = 0,
        Take = 1,
        SalvageForEnergy = 2
    }

    // Use this for initialization
    void Start ()
    {
    }
    
    // Update is called once per frame
    void FixedUpdate ()
    {
        CheckForLoot();
        ProcessPlayerMovement();
        ProcessPlayerLoot();
    }

    void ProcessPlayerMovement()
    {
        //player movement
        float horzAxis = Input.GetAxis("Horizontal");
        float vertAxis = Input.GetAxis("Vertical");
        Rigidbody playerBody = gameObject.rigidbody;

        //calculate the new velocity
        float xVelocity = horzAxis * Time.deltaTime * MovementSpeed;
        float zVelocity = vertAxis * Time.deltaTime * MovementSpeed;
        float yVelocity = playerBody.velocity.y; //we won't have the player jumping so we'll just pass in w/e gravity happens to be doing

        playerBody.velocity = new Vector3(xVelocity, yVelocity, zVelocity);
    }


    void CheckForLoot ()
    {
         //do a radius check, get all loot objects in that radius
        var lootColiders = Physics.OverlapSphere(transform.position, LootRadius, 1 << GlobalProperties.LayerLoot);

        //calculate closest loot
        if (lootColiders.Length > 0)
        {
            ClosestLoot = lootColiders[0].GetComponent<Loot>();
            var dist = Vector3.Distance(transform.position, lootColiders[0].transform.position);

            for (int i = 0; i < lootColiders.Length; i++)
            {
                var tempDist = Vector3.Distance(transform.position, lootColiders[i].transform.position);
                var tempLoot = lootColiders[i].GetComponent<Loot>();

                //auto pickup health
                if (tempLoot.lootType == Loot.LootType.Health && AutoPickUpHealth)
                {
                    ActionLoot(LootAction.Take, tempLoot);
                    continue;
                }

                if (tempDist < dist)
                {
                    ClosestLoot = tempLoot;
                    dist = tempDist;
                }
            }

            HighlightCube.renderer.enabled = true;
            HighlightCube.GetComponent<MatchPosition>().Position = ClosestLoot.transform;
        }
        else
        {
            ClosestLoot = null;
            HighlightCube.renderer.enabled = false;
        }
    }

    void ProcessPlayerLoot ()
    {
        LootAction lootAction = LootAction.None;

        if (Input.GetKeyDown(KeyCode.E) || Input.GetMouseButtonDown(1))
            lootAction = LootAction.Take;

        if (Input.GetKeyDown(KeyCode.Q) || Input.GetMouseButtonDown(2))
            lootAction = LootAction.SalvageForEnergy;
        
        if (lootAction != LootAction.None)
            ActionLoot(lootAction);
    }

    void ActionLoot (LootAction lootAction, Loot loot)
    {
        ClosestLoot = loot;
        ActionLoot(lootAction);
        ClosestLoot = null;
    }

    void ActionLoot(LootAction lootAction)
    {
        if (ClosestLoot != null)
        {
            EntityStats stats = this.GetComponent<EntityStats>();

            if (lootAction == LootAction.Take)
            {
                //replace player's weapon with loot
                if (ClosestLoot.lootType == Loot.LootType.Weapon)
                { 
                    var newWeaponInfo = ClosestLoot.weaponInfo;
                    var currentWeapon = this.GetComponentInChildren<PlayerWeapon>();

                    currentWeapon.info.Damage = newWeaponInfo.Damage;
                    currentWeapon.info.FireRate = newWeaponInfo.FireRate;
                }
                else if (ClosestLoot.lootType == Loot.LootType.Health)
                {
                    stats.health =
                        (stats.health + ClosestLoot.HealthRegain < stats.maxHealth) ? stats.health + ClosestLoot.HealthRegain : stats.maxHealth;
                } else if(ClosestLoot.lootType == Loot.LootType.Secret) {
					int rand = Random.Range(0,3);
					switch(rand) {
						case 0:
							Speed speed = new Speed();
							speed.multiplier = ClosestLoot.SpeedBoost;
							stats.ApplyEffect(speed);
							break;
						case 1:
							Poison poison = new Poison();
							stats.ApplyEffect(poison);
							break;
						case 2:
							EnergyRegen regen = new EnergyRegen();
							regen.duration = 10f;
							regen.regen = 0.2f;
							regen.interval = 0.3f;
							break;
					}
				}
            }
            else if (lootAction == LootAction.SalvageForEnergy)
            {
                //salvage weapon for energy
                stats.shield =
                    (stats.shield + ClosestLoot.EnergySalvage < stats.maxShield) ? stats.shield + ClosestLoot.EnergySalvage : stats.maxShield;
            }

            //add particles to the loot and players
            var lootOut = (Transform)Instantiate(LootOut, ClosestLoot.transform.position, ClosestLoot.transform.rotation);
            lootOut.GetComponent<MatchPosition>().Position = ClosestLoot.transform;
            
            var lootIn = (Transform)Instantiate(LootIn, this.transform.position, this.transform.rotation);
            lootIn.GetComponent<MatchPosition>().Position = this.transform;

            //destroy loot and particles systems when done playing animations
            Destroy(ClosestLoot.gameObject, 0.4f);
            ClosestLoot.gameObject.layer = GlobalProperties.LayerDefault;

            Destroy(lootOut.gameObject, 1.5f);
            Destroy(lootIn.gameObject, 1.5f);
        }
    }
}
