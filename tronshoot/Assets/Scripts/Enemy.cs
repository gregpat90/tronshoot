using UnityEngine;
using System.Collections;
using Pathfinding;
using HutongGames.PlayMaker;

public class Enemy : MonoBehaviour {
    
    private Seeker seeker;
 
    //The calculated path
    public Path path;
    
    //The AI's speed per second
    public float speed = 1;
    
    //The max distance from the AI to a waypoint for it to continue to the next waypoint
    public float nextWaypointDistance = 0.5f;
 
    //The waypoint we are currently moving towards
    private int currentWaypoint = 0;
	
	private PlayMakerFSM fsm;
	
	// Use this for initialization
	void Start () {
		seeker = GetComponent<Seeker>();
		fsm = GetComponent<PlayMakerFSM>();
	}
	
	public void StartPathFinding(){
		Vector3 playerPosition = fsm.FsmVariables.GetFsmVector3("playerPosition").Value;
		
		seeker.StartPath (transform.position, playerPosition, OnPathComplete);
	}
	
	public void OnPathComplete (Path p) {
       // Debug.Log ("Yey, we got a path back. Did it have an error? "+p.error);
        if (!p.error && fsm.ActiveStateName == "Move") {
            path = p;
            //Reset the waypoint counter
            currentWaypoint = 0;
        }
    }
	
	public void StopMovement(){
		path = null;
         //Reset the waypoint counter
        currentWaypoint = 0;
		this.rigidbody.velocity = Vector3.zero;
	}
 
	public void TellOtherEnemiesToAttack(){
		GameObject[] otherEnemies =  GameObject.FindGameObjectsWithTag(this.tag);
		foreach (var enemy in otherEnemies) {
			enemy.GetComponent<PlayMakerFSM>().Fsm.Event("Attack");
		}
		
	}

	public void Shoot(){
		Vector3 playerPosition = fsm.FsmVariables.GetFsmVector3("playerPosition").Value;
		
		Vector3 direction = playerPosition - this.transform.position;
		direction.Normalize();
		
		Laser.Fire(this.transform.position, direction,  GlobalProperties.LayerPlayer, 20);
	}
	
    public void FixedUpdate () {
        if (path == null) {
            //We have no path to move after yet
            return;
        }
        
        if (currentWaypoint >= path.vectorPath.Length) {
            return;
        }
        
        //Direction to the next waypoint
        Vector3 dir = (path.vectorPath[currentWaypoint]-transform.position).normalized;
        dir *= speed * Time.fixedDeltaTime;
		dir.y = 0;
        this.rigidbody.AddForce(dir);
        
        //Check if we are close enough to the next waypoint
        //If we are, proceed to follow the next waypoint
        if (Vector3.Distance (transform.position,path.vectorPath[currentWaypoint]) < nextWaypointDistance) {
            currentWaypoint++;
            return;
        }
    }
}
