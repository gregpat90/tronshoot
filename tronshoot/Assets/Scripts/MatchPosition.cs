using UnityEngine;
using System.Collections;

public class MatchPosition : MonoBehaviour {

    public Transform Position;
	
	// Update is called once per frame
	void Update () {
        if (Position != null)
            this.transform.position = Position.position;
	}
}
