using UnityEngine;
using System.Collections;

public class GlobalProperties : MonoBehaviour {
    public const int LayerDefault = 0;
    public const int LayerPlayer = 8;
    public const int LayerEnemy = 9;
    public const int LayerGui = 10;
    public const int LayerTargetRayCatcher = 11;
    public const int LayerLoot = 12;
    public const string TagTarget = "Target";
    public const string TagPlayerWeapon = "PlayerWeapon";
    public const string TagPlayer = "Player";
    public const int LevelStartScreen = 0;
    public const int LevelMain = 1;
}
