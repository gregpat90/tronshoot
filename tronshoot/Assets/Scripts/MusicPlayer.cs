using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {
    public bool LoopMusic = true;
    public AudioClip StartMusic;
    public float StartMusicVolume;
    public AudioClip LevelOneMusic;
    public float LevelOneMusicVolume;
    private AudioSource audioSource;
    private int currentLevel;

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(this);
        currentLevel = -1;

        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.loop = LoopMusic;
	}
	
	// Update is called once per frame
	void Update () {

        if (Application.loadedLevel != currentLevel)
        {
            currentLevel = Application.loadedLevel;
            LevelChanged();
        }
	}

    void LevelChanged()
    {
        if (audioSource.isPlaying)
            audioSource.Stop();

        switch (currentLevel)
        {
            case 0:
                audioSource.clip = StartMusic;
                audioSource.volume = StartMusicVolume;
                break;
            case 1:
                audioSource.clip = LevelOneMusic;
                audioSource.volume = LevelOneMusicVolume;
                break;
            default:
                break;
        }

        audioSource.Play();
    }

}
