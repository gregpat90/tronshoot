using UnityEngine;
using System.Collections;

public class CameraBounds : MonoBehaviour {
	
	public Color boundColor = Color.white;
	public Transform boundTopLeft;
	public Transform boundBottomRight;
	
	void OnDrawGizmos () {
		if(boundTopLeft != null && boundBottomRight != null) {
			Vector3 topLeft = boundTopLeft.position;
			Vector3 topRight = new Vector3(boundBottomRight.position.x, 0, boundTopLeft.position.z);
			Vector3 bottomLeft = new Vector3(boundTopLeft.position.x, 0, boundBottomRight.position.z);
			Vector3 bottomRight = boundBottomRight.position;
			Gizmos.color = boundColor;
			Gizmos.DrawLine(topLeft, topRight);
			Gizmos.DrawLine(topRight, bottomRight);
			Gizmos.DrawLine(bottomRight, bottomLeft);
			Gizmos.DrawLine(bottomLeft, topLeft);
			Gizmos.color = Color.white;
		}
	}
	
}
