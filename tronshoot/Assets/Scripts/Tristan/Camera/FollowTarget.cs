using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour {
	
	public Transform target;
	public Vector3 offset;
	public float followSpeed = 5;
	public CameraBounds cameraBounds;
//	public Camera camera;

	// Use this for initialization
	void Start () {
		transform.position = target.position + offset;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(target != null) {
			Vector3 move = Vector3.Lerp(transform.position, target.position + offset, followSpeed * Time.deltaTime) - transform.position;
			if(cameraBounds != null) {
//				Vector3 cameraTopLeft = camera.ScreenToWorldPoint(new Vector3(0, camera.pixelHeight, Vector3.Distance(transform.position, target.position)));
//				Vector3 cameraBottomRight = camera.ScreenToWorldPoint(new Vector3(camera.pixelWidth, 0, Vector3.Distance(transform.position, target.position)));
//				Debug.Log(cameraTopLeft);
//				Debug.Log(cameraBottomRight);
				if(	   transform.position.x + move.x < cameraBounds.boundTopLeft.position.x
					|| transform.position.x + move.x > cameraBounds.boundBottomRight.position.x
					|| transform.position.z + move.z > cameraBounds.boundTopLeft.position.z
					|| transform.position.z + move.z < cameraBounds.boundBottomRight.position.z) {
					move = Vector3.zero;
				}
			}
			transform.position += move;
            transform.LookAt(target,Vector3.up);
		}
	}
}
