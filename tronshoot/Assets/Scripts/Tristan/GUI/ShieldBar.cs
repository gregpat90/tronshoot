using UnityEngine;
using System.Collections;

public class ShieldBar : GUIStatBar {
	
	public EntityStats stats;
	
	protected override void Start() {
		if(stats != null)
			this.maxValue = stats.maxShield;
		base.Start();
	}
	
	protected override void Update() {
		if(stats != null)
			this.targetValue = stats.shield;
		base.Update();
	}
	
}
