using UnityEngine;
using System.Collections;

public class EquippedGUI : MonoBehaviour {
	
	public TextMesh title;
	public TextMesh desc1;
	public TextMesh desc2;
	private GameObject player;
	
	void Start() {
		player = GameObject.FindGameObjectWithTag("Player");
		if(player == null) {
			Debug.LogError("EquippedGUI: Player not found.");
			gameObject.SetActiveRecursively(false);
		}
	}
	
	void Update () {
		PlayerWeapon weapon = player.GetComponentInChildren<PlayerWeapon>();
		if(weapon != null) {
			desc1.text = "Damage: " + weapon.info.Damage;
			desc2.text = "Fire Rate: " + weapon.info.FireRate;
		}
	}
}
