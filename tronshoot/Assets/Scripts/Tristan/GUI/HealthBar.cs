using UnityEngine;
using System.Collections;

public class HealthBar : GUIStatBar {
	
	public EntityStats stats;
	
	protected override void Start() {
		this.maxValue = stats.maxHealth;
		base.Start();
	}
	
	protected override void Update() {
		this.targetValue = stats.health;
		base.Update();
	}
	
}
