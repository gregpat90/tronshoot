using UnityEngine;
using System.Collections;

public class GUIStatBar : MonoBehaviour {
	
	public GameObject foreground;
	public GameObject main;
	public GameObject background;
    public float barFLowSpeed = 0.5f;
    protected float targetValue;
	protected float currentValue;
	protected float maxValue;
	
	private Vector3 initialScale;
	private float initialLeft;
	
	protected virtual void Start() {
		if(main != null) {
			initialScale = main.transform.localScale;
            targetValue = maxValue;
		}
	}
	
	protected virtual void Update() {
		if(main != null) {
            currentValue = Mathf.Lerp(currentValue, targetValue, barFLowSpeed);
            main.transform.localScale = new Vector3((initialScale.x * (currentValue / maxValue)), initialScale.y, initialScale.z);
		}
	}
	
}
