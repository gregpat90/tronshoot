using UnityEngine;
using System.Collections;

public class SalvageMouseDown : MonoBehaviour {
	
	private LootGUI lootGui;
	
	void Start() {
		if(transform.parent != null) {
			lootGui = transform.parent.GetComponent<LootGUI>();
			if(lootGui == null) {
				Debug.LogError("SalvageMouseDown: Collider must be child of LootGUI.");
			}
		}
	}
	
	void OnMouseDown() {
		lootGui.PerformAction(PCPlayerController.LootAction.SalvageForEnergy);
	}
	
}
