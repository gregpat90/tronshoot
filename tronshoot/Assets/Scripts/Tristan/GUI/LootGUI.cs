using UnityEngine;
using System.Collections;

public class LootGUI : MonoBehaviour {
	
	public TextMesh title;
	public TextMesh desc1;
	public TextMesh desc2;
	public TextMesh equipBtn;
	public GameObject icon;
	public Material weaponMaterial;
	public Material healthMaterial;
	public Material energyMaterial;
	public Camera guiCam;
	public Vector3 lootOffset;
	
	private GameObject player;
	
	void Start() {
		player = GameObject.FindGameObjectWithTag("Player");
		if(player == null) {
			Debug.LogError("LootGUI: Player not found.");
			gameObject.SetActiveRecursively(false);
		}
	}
	
	void Update() {
		PCPlayerController pcpc = player.GetComponent<PCPlayerController>();
		if(pcpc != null && pcpc.ClosestLoot != null) {
			gameObject.SetActiveRecursively(true);
			Loot loot = pcpc.ClosestLoot;
			
			transform.localPosition = new Vector3(1.634289f, -5.56978f, 20.6139f);
//			// Move this object above the loot.
//			Vector3 mainCameraScreenPos = Camera.mainCamera.WorldToScreenPoint(loot.transform.position);
//			mainCameraScreenPos.z = 1f;
//			Vector3 guiCameraWorldPos = guiCam.ScreenToWorldPoint(mainCameraScreenPos);
//			Vector3 temp = guiCameraWorldPos + lootOffset;
//			temp.z = 20;
//			this.transform.localPosition =  temp;
			
			title.text = loot.lootType.ToString();
			switch(loot.lootType) {
				case Loot.LootType.Weapon:
					icon.renderer.material = weaponMaterial;
					WeaponInfo info = loot.weaponInfo;
					desc1.text = "Damage: " + info.Damage;
					desc2.text = "Fire Rate: " + info.FireRate;
					
					break;
				case Loot.LootType.Health:
					icon.renderer.material = healthMaterial;
					desc1.text = "Heals " + loot.HealthRegain + " health.";
					break;
			}
		} else {
			transform.localPosition = new Vector3(0, 0, 6);
		}
	}
	
	public void PerformAction(PCPlayerController.LootAction lootAction) {
		player.BroadcastMessage("ActionLoot", lootAction);
	}
	
}
