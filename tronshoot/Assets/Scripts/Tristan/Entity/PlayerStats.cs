using UnityEngine;
using System.Collections;

public class PlayerStats : EntityStats {
	
	void Start() {
		EnergyRegenInfinite regen = new EnergyRegenInfinite();
		regen.regen = 0.5f;
		regen.interval = 0.3f;
		this.ApplyEffect(regen);
	}
	
}
