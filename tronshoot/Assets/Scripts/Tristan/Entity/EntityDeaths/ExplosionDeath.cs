using UnityEngine;
using System.Collections;

public class ExplosionDeath : EntityDeath {
	
	public GameObject explosion;
    public AudioClip deathSound;
	
	public override void Execute() {
		
		Loot.SpawnRandom(this.transform.position);
		Destroy(gameObject);

		// Create the explosion
        if (explosion != null)
        {
            // Only instantiate if there is an explosion object.
            GameObject go = Instantiate(explosion, this.transform.position, this.transform.rotation) as GameObject;
			go.GetComponent<Detonator>().Explode();

            //play death sound if set
            if (deathSound != null)
            {
                var audioSource = go.AddComponent<AudioSource>();
                audioSource.clip = deathSound;
				audioSource.volume = 1.0f;
                audioSource.loop = false;
                audioSource.Play();
            }

            go.transform.parent = null; // Add to container?
        }
	}
	
}
