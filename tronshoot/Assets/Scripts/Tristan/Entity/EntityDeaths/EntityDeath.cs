using UnityEngine;
using System.Collections;

public abstract class EntityDeath : MonoBehaviour {
	
	/// <summary>
	/// This will be executed apon an entity's death when this script is attached to an entity.
	/// </summary>
	public abstract void Execute();
	
}
