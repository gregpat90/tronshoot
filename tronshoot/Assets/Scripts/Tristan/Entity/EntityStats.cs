using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EntityStats : MonoBehaviour {
	
	public float health = 100;
	public float maxHealth = 100;
	public float shield = 100;
	public float maxShield = 100;
	public float movement = 300;
	
	private bool alive = true;
	private List<StatEffect> statEffects = new List<StatEffect>();
	
    /// <summary>
    /// Updates every frame.
    /// </summary>
	void Update() {
		List<StatEffect> toRemove = new List<StatEffect>();
		foreach(StatEffect effect in statEffects) {
			effect.Apply(this);
			if(effect.Inactive()) {
				toRemove.Add(effect);
			}
		}
		foreach(StatEffect effect in toRemove) {
			effect.End(this);
			statEffects.Remove(effect);
		}
		if(health <= 0 && alive) {
			health = 0;
			Kill();
		}
	}
	
    /// <summary>
    /// Applies the effect to this entity. This also calls the Start function within the effect.
    /// </summary>
    /// <param name="effect">The effect being applied.</param>
	public void ApplyEffect(StatEffect effect) {
        if(effect.Start(this))
		    statEffects.Add(effect);
	}

    /// <summary>
    /// Removes the first instance of the effect with the given type.
    /// </summary>
    /// <param name="type">The class type of the effect.</param>
    public void RemoveEffect(System.Type type)
    {
        StatEffect toRemove = null;
        foreach (StatEffect effect in statEffects)
        {
            if (effect.GetType() == type)
            {
                toRemove = effect;
                break;
            }
        }
        if (toRemove != null)
        {
            toRemove.End(this);
            statEffects.Remove(toRemove);
        }
    }

    /// <summary>
    /// Removes all instances of the effect with the given type. To remove all use StatEffect as the type.
    /// </summary>
    /// <param name="type">The class type of the effect.</param>
    public void RemoveEffects(System.Type type)
    {
        List<StatEffect> toRemove = new List<StatEffect>();
        foreach (StatEffect effect in statEffects)
        {
            if (effect.GetType() == type)
            {
                toRemove.Add(effect);
            }
        }
        if (toRemove.Count > 0)
        {
            foreach (StatEffect effect in toRemove)
            {
                effect.End(this);
                statEffects.Remove(effect);
            }
        }
    }
	
    /// <summary>
    /// Applys damage to first the shield then the health.
    /// </summary>
    /// <param name="amount">The amount of damage.</param>
	public void ApplyDamage(float amount) {
		float damageLeft = amount - shield;
		shield -= amount;
		if(shield < 0) {
			shield = 0;
		}
		if(damageLeft > 0) {
			health -= damageLeft;	
		}
		
		PlayMakerFSM fsm = this.GetComponent<PlayMakerFSM>();
		if(fsm != null){
			fsm.Fsm.Event("Hit"); 
		}
		
		if(health <= 0 && alive) {
			health = 0;
			// Trigger Death
			Kill();
		}
	}
	
    /// <summary>
    /// Kills the entity and calls the death script attached to the entity. If no script is attached, it will send a warning.
    /// </summary>
	public void Kill() {
		alive = false;
		health = 0;
		// Do other stuff
		EntityDeath entityDeath = gameObject.GetComponent(typeof(EntityDeath)) as EntityDeath;
		if(entityDeath != null) {
			entityDeath.Execute();
		} else {
			Debug.LogWarning("Entity died but no death script was attached.");
		}
	}
	
}
