using UnityEngine;
using System.Collections;

public abstract class StatEffect {

    /// <summary>
    /// Overridable function that is called when the effect is added to an entity.
    /// </summary>
    /// <param name="stats">The entity's stats to effect.</param>
    /// <returns>True when the effect can/was successfully applied.</returns>
    public abstract bool Start(EntityStats stats);

    /// <summary>
    /// Overridable function that is called every frame when attached to an entity.
    /// </summary>
    /// <param name="stats">The entity's stats to effect.</param>
    public abstract void Apply(EntityStats stats);

    /// <summary>
    /// Overridable function that is called when the effect is removed from an entity.
    /// </summary>
    /// <param name="stats">The entity's stats to effect.</param>
    public abstract void End(EntityStats stats);
	
	public abstract bool Inactive();
	
}
