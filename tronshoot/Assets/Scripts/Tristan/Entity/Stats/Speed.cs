using UnityEngine;
using System.Collections;

public class Speed : StatEffect {
	
	public float duration = 10f;
    public float multiplier = 1.5f;
	private float expire;

    public override bool Start(EntityStats stats)
    {
		expire = Time.time + duration;
        stats.movement *= multiplier;
        return true;
    }

    public override void Apply(EntityStats stats)
    {
    }

    public override void End(EntityStats stats)
    {
        stats.movement /= multiplier;
    }
	
	public override bool Inactive ()
	{
		return Time.time > expire;
	}
}
