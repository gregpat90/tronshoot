using UnityEngine;
using System.Collections;

public class EnergyRegen : StatEffect {
	
	public float duration = 10f;
	public float regen = 0.2f;
	public float interval = 0.3f;
	private float expire;
	private float lastApplied;
	
	public override bool Start (EntityStats stats)
	{
		expire = Time.time + duration;
		return true;
	}
	
	public override void Apply (EntityStats stats)
	{
		if(Time.time - lastApplied > interval) {
			stats.shield += regen;
			if(stats.shield > stats.maxShield) {
				stats.shield = stats.maxShield;
			}
			lastApplied = Time.time;
		}
	}
	
	public override void End (EntityStats stats)
	{
	}
	
	public override bool Inactive ()
	{
		return Time.time > expire;
	}
	
}
