using UnityEngine;
using System.Collections;

public class Poison : StatEffect {
	
	public float duration = 10f;
	public float damage = 2f;
	public float interval = 1f;
	private float lastApplied;
	private float expire;

    public override bool Start(EntityStats stats)
    {
		expire = Time.time + duration;
        // Let the player know they have been poisoned?
        // Perhaps a gui change.
        return true; // This indicates to the entity stats that the effect has/can be applied.
    }
	
	public override void Apply(EntityStats stats) {
		if(Time.time - lastApplied > interval) {
			stats.health -= damage;
			if(stats.health < 0) {
				stats.health = 0;
			}
			lastApplied = Time.time;
		}
	}

    public override void End(EntityStats stats)
    {
        // Undo any gui changes etc..
    }
	
	public override bool Inactive ()
	{
		return Time.time > expire;
	}
}
