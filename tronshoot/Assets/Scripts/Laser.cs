using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour {
	
	public Vector3 start;
	public Vector3 end;
	
	public Vector3 direction;
	public float length = 5;
	
	public float speed = 50.0f;
	
	private LineRenderer line;
	private float lifeTime = 0;
	private float maxLifeTime = 1.0f;
	public int layerMask = 0;
	public int damage = 0;

    public AudioClip FireSound;
	// Use this for initialization
	void Start() {
		this.line = (LineRenderer)GetComponent("LineRenderer");

        //play death sound if set
        if (FireSound != null)
        {
            var audioSource = this.gameObject.AddComponent<AudioSource>();
            audioSource.clip = FireSound;
			audioSource.volume = 1.0f;
            audioSource.loop = false;
            audioSource.Play();
        }
	}
	
	public static void Fire(Vector3 start, Vector3 direction, int layer, int damage){
	
		GameObject go = Instantiate(Resources.Load("Laser-Pre")) as GameObject;
		
		Laser laser = (Laser)go.GetComponent("Laser");	
		LineRenderer line = go.GetComponent("LineRenderer") as LineRenderer;
		laser.layerMask = 1 << layer;
		//laser.layerMask = laser.layerMask << 13;
		laser.start = start;
		laser.damage = damage;
		laser.direction = direction;
		
		//line.useWorldSpace = false; 
		
		line.SetPosition(0,laser.start);
		
		laser.end = laser.start + (laser.direction * laser.length);
		
		line.SetPosition(1,laser.end);
	}
	
	// Update is called once per frame
	void Update () {
		
			this.lifeTime += Time.deltaTime;
		
		
			this.end += (this.direction * Time.deltaTime * speed );
			this.start += (this.direction * Time.deltaTime * speed);
			
			this.line.SetPosition(0,this.start);
			this.line.SetPosition(1,this.end);
		
			if(this.lifeTime > this.maxLifeTime)
			{
				Destroy(this.gameObject);
			}

			RaycastHit hit;
			if (Physics.Raycast(this.start, this.direction, out hit, this.length, this.layerMask))
			{
				Destroy(this.gameObject);
				print ("Laser Hit!");
				if(hit.collider.gameObject.GetComponent<EntityStats>() != null){
					hit.collider.gameObject.GetComponent<EntityStats>().ApplyDamage(this.damage);
				}
			}
	}
}
