using UnityEngine;
using System.Collections;

public class PlayerWeapon : MonoBehaviour {
    public WeaponInfo info;
	private PlayerStats stats;
	private float weaponEnergyUse = 2;
    private GameObject target;
    private float lastFire;
	private Transform projectileOrigin;

    // Use this for initialization
    void Start () {
		stats = transform.parent.GetComponent<PlayerStats>();
        target = GameObject.FindWithTag(GlobalProperties.TagTarget);
		
        info = new WeaponInfo();
		info.FireRate = 0.3f;
		info.Damage = 20;
        lastFire = 0.0f;
		
		this.projectileOrigin = this.transform.FindChild("ProjectileOrigin").transform;
		
    }
    
    // Update is called once per frame
    void Update () {
        //point at target

        Vector3 lookTarget = new Vector3
        {
            x = target.transform.position.x,
            y = this.transform.position.y,
            z = target.transform.position.z
        };

        transform.LookAt(lookTarget, Vector3.up);
    }

    void FireTurret()
    {
        if (Time.time > lastFire + info.FireRate && stats.shield >= weaponEnergyUse)
        {
			stats.shield -= weaponEnergyUse;
            Laser.Fire(projectileOrigin.transform.position,
                projectileOrigin.transform.forward, GlobalProperties.LayerEnemy, this.info.Damage);
            lastFire = Time.time;
        }
    }
}
