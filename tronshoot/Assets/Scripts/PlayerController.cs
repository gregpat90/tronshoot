using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

public class PlayerController : MonoBehaviour {
	
	Vector2 touchShootDelta =  new Vector2(0,0);
	Vector2 touchMoveDelta = new Vector2(0,0);
	
	int shootFingerId = -1;
	int moveFingerId = -1;
	
	int touchSize = 200;
	
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		foreach(Touch touch in Input.touches){	
			
			if(touch.phase == TouchPhase.Began){
				
				if(touch.position.x <= touchSize && touch.position.y <= touchSize && shootFingerId == -1){
					shootFingerId = touch.fingerId;
					touchShootDelta.x = 0;
					touchShootDelta.y = 0;
				}
				
				if(touch.position.x >= (Camera.main.pixelWidth - touchSize) && touch.position.y <= touchSize && moveFingerId == -1){
					moveFingerId = touch.fingerId;
					touchMoveDelta.x = 0;
					touchMoveDelta.y = 0;
					this.rigidbody.velocity = Vector3.zero;
				}
			}
			
			if(touch.phase == TouchPhase.Moved){
				
				if(touch.fingerId == moveFingerId){
					touchMoveDelta += touch.deltaPosition;
				}
				
				if(touch.fingerId == shootFingerId){
					touchShootDelta += touch.deltaPosition;
				}
				
			}
			
			if(touch.phase == TouchPhase.Ended){
				
				if(touch.fingerId == shootFingerId){
					shootFingerId = -1;	
					touchShootDelta.Normalize();
					FireLaser(touchShootDelta);
				}
				
				if(touch.fingerId == moveFingerId){
					moveFingerId = -1;
					touchMoveDelta.Normalize();
					touchMoveDelta *= 20.0f;
					this.rigidbody.AddForce(touchMoveDelta.x, 0, touchMoveDelta.y, ForceMode.Impulse);
				}
				
				//this.transform.Translate(touchDelta.x, 0, touchDelta.y, Space.World);
			}
			
			if(touch.phase == TouchPhase.Canceled){
				if(touch.fingerId == shootFingerId){
					shootFingerId = -1;
				}
				
				if(touch.fingerId == moveFingerId){
					moveFingerId = -1;
				}
			}
		}
		#if UNITY_EDITOR
		/*
			if(Input.GetMouseButton(0)){
				FireLaser(Vector2.one);
			}
			
			if(Input.GetKey(KeyCode.UpArrow)){
				this.rigidbody.AddForce(0, 0, 0.4f, ForceMode.Impulse);
			}
			if(Input.GetKey(KeyCode.DownArrow)){
				this.rigidbody.AddForce(0, 0,-0.4f, ForceMode.Impulse);
			}
		
			if(Input.GetKey(KeyCode.RightArrow)){
				this.rigidbody.AddForce(0.4f, 0, 0, ForceMode.Impulse);
			}
			if(Input.GetKey(KeyCode.LeftArrow)){
				this.rigidbody.AddForce(-0.4f, 0, 0, ForceMode.Impulse);
			}
			*/
		#endif
		
	
	}
	
	void FireLaser(Vector2 direction){
		Laser.Fire(this.transform.position, new Vector3(direction.x,0,direction.y), 9, 10);
	}
	
	void OnDrawGizmos(){
		// Draws a blue line from this transform to the target
        Gizmos.color = Color.red;

		Vector3 left = Camera.main.ScreenToWorldPoint( new Vector3(0, touchSize, Camera.main.nearClipPlane));
		Vector3 right = Camera.main.ScreenToWorldPoint( new Vector3(touchSize, touchSize, Camera.main.nearClipPlane));
		Vector3 bottom = Camera.main.ScreenToWorldPoint( new Vector3(touchSize, 0, Camera.main.nearClipPlane));
		
        Gizmos.DrawLine (left, right);
		Gizmos.DrawLine (right, bottom);
		
		
		left = Camera.main.ScreenToWorldPoint( new Vector3(Camera.main.pixelWidth - touchSize, touchSize, Camera.main.nearClipPlane));
		right = Camera.main.ScreenToWorldPoint( new Vector3((Camera.main.pixelWidth), touchSize, Camera.main.nearClipPlane));
		bottom = Camera.main.ScreenToWorldPoint( new Vector3((Camera.main.pixelWidth - touchSize), 0, Camera.main.nearClipPlane));
		
        Gizmos.DrawLine (left, right);
		Gizmos.DrawLine (left, bottom);
		
	}
}
