using UnityEngine;
using System.Collections;

public class Flash : MonoBehaviour {

    public float ChangeTime = 1.5f;
    private float lastChange;
	
	// Update is called once per frame
	void Update () {
        if (Time.time > lastChange + ChangeTime)
        {
            this.renderer.enabled = !this.renderer.enabled;
            lastChange = Time.time;
        }
	}
}
