using UnityEngine;
using System.Collections;

public class PlayerWeaponController : MonoBehaviour {
    private GameObject[] weapons;

    // Use this for initialization
    void Start () {
        AllocateWeapons();

        //add PlayerWeapon script to each weapon found
        foreach (var weapon in weapons)
        {
            weapon.AddComponent("PlayerWeapon");
        }
    }
    
    // Update is called once per frame
    void Update () {
        //fire a turret on left click or while left button is down
        if (Input.GetMouseButton(0)) //left button is down
        {
            BroadcastMessage("FireTurret");
        }

    }

    void AllocateWeapons()
    {
        weapons = GameObject.FindGameObjectsWithTag(GlobalProperties.TagPlayerWeapon);
    }
}
